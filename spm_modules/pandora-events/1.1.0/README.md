# pandora-events [![spm version](http://127.0.0.1:3000/badge/pandora-events)](http://127.0.0.1:3000/package/pandora-events)

---



## Install

```
$ spm install pandora-events --save
```

## Usage

```js
var pandoraEvents = require('pandora-events');
// use pandoraEvents
```
